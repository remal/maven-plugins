package name.remal.maven.root_basedir_property;

import lombok.RequiredArgsConstructor;
import lombok.val;
import org.apache.maven.AbstractMavenLifecycleParticipant;
import org.apache.maven.execution.MavenSession;
import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.logging.Logger;

import javax.inject.Inject;
import java.util.Objects;

import static java.lang.String.format;
import static name.remal.maven.utils.MavenSessionUtils.*;

@Component(
    role = AbstractMavenLifecycleParticipant.class,
    hint = "set-root-basedir-property"
)
@RequiredArgsConstructor(onConstructor_ = {@Inject})
public class RootBasedirPropertyExtension extends AbstractMavenLifecycleParticipant {

    public static final String ROOT_BASEDIR_PROPERTY = "root-basedir";
    public static final String IS_ROOT_PROJECT_PROPERTY = "is-root-project";
    public static final String IS_NOT_ROOT_PROJECT_PROPERTY = "is-not-root-project";

    private final Logger logger;

    @Override
    public void afterProjectsRead(MavenSession session) {
        logger.debug("Calculating root directory...");
        val rootBasedir = getRootBasedir(session);
        logger.info(format("%s: %s", ROOT_BASEDIR_PROPERTY, rootBasedir));
        setProperty(session, ROOT_BASEDIR_PROPERTY, rootBasedir.getPath());

        val rootProject = getRootProject(session);
        forEachProject(session, project -> {
            boolean isRootProject = Objects.equals(project, rootProject);
            project.getProperties().put(IS_ROOT_PROJECT_PROPERTY, "" + isRootProject);
            project.getProperties().put(IS_NOT_ROOT_PROJECT_PROPERTY, "" + !isRootProject);
        });
    }

}
