package name.remal.maven.utils;

import lombok.SneakyThrows;
import lombok.val;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;

import static java.util.Comparator.comparing;
import static name.remal.maven.utils.MavenProjectUtils.getAbsoluteBasedir;
import static name.remal.maven.utils.MavenProjectUtils.getRequiredAbsoluteBasedir;

public interface MavenSessionUtils {

    static void forEachProject(MavenSession session, Consumer<MavenProject> action) {
        Set<MavenProject> processedProjects = new HashSet<>();
        Queue<MavenProject> projectsToProcess = new LinkedList<>(session.getAllProjects());
        while (true) {
            val project = projectsToProcess.poll();
            if (project == null) break;
            if (!processedProjects.add(project)) continue;

            Optional.ofNullable(project.getParent()).ifPresent(projectsToProcess::add);

            action.accept(project);
        }
    }

    @SneakyThrows
    static MavenProject getRootProject(MavenSession session) {
        SortedMap<Path, List<MavenProject>> basedirsMap = new TreeMap<>(comparing(path -> {
            if (OS.isWindows) {
                return path.toString().toLowerCase();
            } else {
                return path.toString();
            }
        }));
        forEachProject(session, project -> {
            val basedir = getAbsoluteBasedir(project);
            if (basedir != null) {
                basedirsMap.computeIfAbsent(basedir.toPath(), __ -> new ArrayList<>()).add(project);
            }
        });
        if (basedirsMap.isEmpty()) {
            throw new MojoExecutionException("No project base directories found! Are you sure you're executing this on a valid Maven project?");
        }

        List<Path> basedirs = new ArrayList<>(basedirsMap.keySet());
        val rootBasedir = basedirs.get(0);
        val rootProjects = basedirsMap.get(rootBasedir);
        if (rootProjects.size() != 1) {
            throw new MojoExecutionException("${rootProjects.size} projects has the same base directory: " + rootBasedir);
        }

        val nextBasedir = basedirs.size() >= 2 ? basedirs.get(1) : null;
        if (nextBasedir != null) {
            if (!nextBasedir.startsWith(rootBasedir)) {
                throw new MojoExecutionException("Cannot find a single highest directory for this project set. First two candidates directories don't share a common root.");
            }
        }

        return rootProjects.get(0);
    }

    static File getRootBasedir(MavenSession session) {
        return getRequiredAbsoluteBasedir(getRootProject(session));
    }

    static void setProperty(MavenSession session, String key, String value) {
        session.getSystemProperties().setProperty(key, value);
        forEachProject(session, project -> {
            project.getProperties().setProperty(key, value);
        });
    }

}
