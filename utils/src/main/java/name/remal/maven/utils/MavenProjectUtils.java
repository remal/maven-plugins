package name.remal.maven.utils;

import lombok.SneakyThrows;
import name.remal.maven.annotations.Nullable;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.util.Optional;

public interface MavenProjectUtils {

    @Nullable
    static File getAbsoluteBasedir(MavenProject project) {
        return Optional.ofNullable(project.getBasedir()).map(File::getAbsoluteFile).orElse(null);
    }

    @SneakyThrows
    static File getRequiredAbsoluteBasedir(MavenProject project) {
        File result = getAbsoluteBasedir(project);
        if (result == null) throw new MojoExecutionException("Project doesn't have basedir set: " + project);
        return result;
    }

}
