package name.remal.maven.utils;

import lombok.SneakyThrows;
import name.remal.maven.annotations.Nullable;

import java.beans.PropertyDescriptor;
import java.io.*;
import java.lang.reflect.Method;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.util.*;
import java.util.stream.Stream;

import static java.beans.Introspector.getBeanInfo;
import static java.nio.file.Files.*;
import static java.util.Arrays.asList;
import static java.util.Collections.newSetFromMap;
import static java.util.Comparator.comparing;
import static java.util.Map.Entry;
import static java.util.stream.Collectors.toList;
import static name.remal.maven.utils.Constants.UTILS_VERSION;
import static org.apache.commons.codec.binary.Hex.encodeHexString;
import static org.apache.commons.codec.digest.DigestUtils.getSha512Digest;

public interface StateUtils {

    @SuppressWarnings("unchecked")
    @SneakyThrows
    static String hashState(@Nullable Object... objects) {
        MessageDigest digest = getSha512Digest();

        Set<Path> processedPaths = new HashSet<>();
        Set<Object> processedObjects = newSetFromMap(new IdentityHashMap<>());
        Queue<Object> objectsQueue = new LinkedList<>();
        objectsQueue.add(UTILS_VERSION);
        if (objects != null) {
            objectsQueue.addAll(asList(objects));
        }
        while (!objectsQueue.isEmpty()) {
            Object object = objectsQueue.poll();
            if (object == null) {
                digest.update((byte) 0);
                continue;
            }

            if (object instanceof File) {
                File file = (File) object;
                objectsQueue.add(file.toPath());
                continue;
            }
            if (object instanceof Path) {
                Path rootPath = (Path) object;
                rootPath = rootPath.toAbsolutePath();
                if (processedPaths.add(rootPath)) {
                    if (isRegularFile(rootPath)) {
                        objectsQueue.add(rootPath.getParent());
                    } else if (exists(rootPath)) {
                        try (Stream<Path> pathsStream = walk(rootPath)) {
                            List<Path> paths = pathsStream.sorted().collect(toList());
                            for (Path path : paths) {
                                objectsQueue.add(path.toString());
                                try (InputStream inputStream = newInputStream(path)) {
                                    byte[] buffer = new byte[8192];
                                    int read;
                                    while (0 <= (read = inputStream.read(buffer))) {
                                        digest.update(buffer, 0, read);
                                    }
                                }
                            }
                        }
                    } else {
                        objectsQueue.add(rootPath.toString());
                    }
                }
                continue;
            }

            if (object instanceof Iterable) {
                Iterable iterable = (Iterable) object;
                iterable.forEach(objectsQueue::add);
                continue;
            }
            if (object instanceof Map) {
                Map map = (Map) object;
                objectsQueue.addAll(map.entrySet());
                continue;
            }
            if (object instanceof Entry) {
                Entry entry = (Entry) object;
                objectsQueue.add(entry.getKey());
                objectsQueue.add(entry.getValue());
                continue;
            }
            if (object instanceof Optional) {
                Optional optional = (Optional) object;
                objectsQueue.add(optional.orElse(null));
                continue;
            }

            if (object instanceof Serializable) {
                try (ByteArrayOutputStream bytesOutputStream = new ByteArrayOutputStream()) {
                    try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(bytesOutputStream)) {
                        objectOutputStream.writeObject(object);
                    }
                    digest.update(bytesOutputStream.toByteArray());
                }
                continue;
            }

            if (processedObjects.add(object)) {
                List<PropertyDescriptor> descriptors = Stream.of(getBeanInfo(object.getClass()).getPropertyDescriptors())
                    .filter(desc -> desc.getReadMethod() != null)
                    .sorted(comparing(PropertyDescriptor::getName))
                    .collect(toList());
                for (PropertyDescriptor descriptor : descriptors) {
                    Method readMethod = descriptor.getReadMethod();
                    readMethod.setAccessible(true);
                    Object value = readMethod.invoke(object);
                    objectsQueue.add(value);
                }
            }
        }

        return encodeHexString(digest.digest());
    }

}
