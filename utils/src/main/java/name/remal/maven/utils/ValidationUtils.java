package name.remal.maven.utils;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static javax.validation.Validation.buildDefaultValidatorFactory;

public interface ValidationUtils {

    static void validateObject(Object object) {
        Stream.of(
            "org.hibernate.validator.internal.util.Version"
        ).forEach(it -> Logger.getLogger(it).setLevel(Level.WARNING));

        Set<ConstraintViolation<Object>> violations = buildDefaultValidatorFactory().getValidator().validate(object);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }

}
