package name.remal.maven.utils;

public interface OS {

    boolean isWindows = System.getProperty("os.name", "").toLowerCase().startsWith("windows");

}
