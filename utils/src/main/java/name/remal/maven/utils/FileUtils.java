package name.remal.maven.utils;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Contract;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import static java.nio.file.FileVisitResult.CONTINUE;

public interface FileUtils {

    @SneakyThrows
    @Contract("_ -> param1")
    static File createDirectories(File directory) {
        Files.createDirectories(directory.toPath().toAbsolutePath());
        return directory;
    }

    @SneakyThrows
    @Contract("_ -> param1")
    static File createParentDirectories(File file) {
        File directory = file.getAbsoluteFile().getParentFile();
        if (directory != null) {
            Files.createDirectories(directory.toPath());
        }
        return file;
    }

    @SneakyThrows
    @Contract("_ -> param1")
    static File forceDeleteRecursively(File file) {
        if (file.exists()) {
            Files.walkFileTree(file.toPath().toAbsolutePath(), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.deleteIfExists(file);
                    return CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException e) throws IOException {
                    if (e != null) {
                        throw e;
                    }
                    Files.deleteIfExists(dir);
                    return CONTINUE;
                }
            });
        }
        return file;
    }

}
