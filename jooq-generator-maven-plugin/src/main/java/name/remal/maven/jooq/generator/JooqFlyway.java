package name.remal.maven.jooq.generator;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static lombok.AccessLevel.PRIVATE;

@Data
@NoArgsConstructor
@FieldDefaults(level = PRIVATE)
public class JooqFlyway {

    @NotNull
    List<String> locations = new ArrayList<>();

    @NotNull
    Map<String, String> placeholders = new HashMap<>();

    @NotNull
    List<String> schemas = new ArrayList<>();

    @NotEmpty
    String historyTableName = "flyway_schema_history";

}
