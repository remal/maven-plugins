package name.remal.maven.jooq.generator;

import liquibase.Liquibase;
import liquibase.change.Change;
import liquibase.changelog.ChangeSet;
import liquibase.changelog.DatabaseChangeLog;
import liquibase.changelog.visitor.AbstractChangeExecListener;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.FileSystemResourceAccessor;
import liquibase.resource.ResourceAccessor;
import lombok.SneakyThrows;
import name.remal.maven.annotations.Nullable;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.logging.Logger;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;
import org.jooq.codegen.GenerationTool;
import org.jooq.meta.jaxb.Configuration;
import org.jooq.meta.jaxb.Database;
import org.jooq.meta.jaxb.Generator;
import org.jooq.meta.jaxb.Target;
import org.testcontainers.containers.JdbcDatabaseContainer;
import org.testcontainers.containers.JdbcDatabaseContainerProvider;

import javax.sql.DataSource;
import javax.validation.Valid;
import java.io.File;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.lang.String.format;
import static java.lang.String.join;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.Files.readAllBytes;
import static java.nio.file.Files.write;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.joining;
import static name.remal.maven.jooq.generator.Constants.PLUGIN_ARTIFACT_ID;
import static name.remal.maven.utils.FileUtils.*;
import static name.remal.maven.utils.StateUtils.hashState;
import static name.remal.maven.utils.ValidationUtils.validateObject;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.maven.plugins.annotations.LifecyclePhase.GENERATE_SOURCES;

@Mojo(name = "jooq-generate-sources", defaultPhase = GENERATE_SOURCES)
public class JooqGenerateSourcesMojo extends AbstractMojo {

    @Valid
    @Parameter
    private List<JooqGeneration> generations = new ArrayList<>();

    @Parameter(required = true, defaultValue = "${project.build.directory}/generated-sources/jooq")
    @SuppressWarnings("NullableProblems")
    private File outputDirectory;

    @Parameter(required = true, defaultValue = "${project.build.sourceEncoding}")
    @SuppressWarnings("NullableProblems")
    private String migrationsEncoding;

    @Parameter(required = true, defaultValue = "${project.build.sourceEncoding}")
    @SuppressWarnings("NullableProblems")
    private String generatedSourcesEncoding;

    @Parameter(property = "jooq-generate-sources.skip", defaultValue = "false")
    private boolean skip;


    @Parameter(required = true, readonly = true, defaultValue = "${project}")
    @SuppressWarnings("NullableProblems")
    private MavenProject project;


    @Component
    @SuppressWarnings("NullableProblems")
    private Logger logger;


    @Override
    public void execute() throws MojoExecutionException {
        try {
            if (skip) {
                logger.info("Skipping plugin execution");
                return;
            }

            validateObject(this);

            String stateHash = hashState(generations, outputDirectory, migrationsEncoding, generatedSourcesEncoding);
            logger.debug("Current state hash: " + stateHash);
            File stateHashFile = new File(project.getBuild().getDirectory(), PLUGIN_ARTIFACT_ID + "-markers/state.hash");
            String prevStateHash = stateHashFile.isFile() ? new String(readAllBytes(stateHashFile.toPath()), UTF_8) : null;
            logger.debug("Previous state hash: " + prevStateHash);
            if (stateHash.equals(prevStateHash)) {
                logger.info("Nothing to generate - all sources are up to date");
                return;
            } else {
                createParentDirectories(stateHashFile);
                byte[] bytes = stateHash.getBytes(UTF_8);
                write(stateHashFile.toPath(), bytes);
            }

            for (JooqGeneration generation : generations) {
                generate(generation);
            }

        } catch (Throwable e) {
            //noinspection ConstantConditions
            if (e instanceof MojoExecutionException) {
                throw (MojoExecutionException) e;
            }
            throw new MojoExecutionException(e.getMessage(), e);
        }
    }


    @SneakyThrows
    private void generate(JooqGeneration generation) {
        JdbcDatabaseContainer databaseContainer = newDatabaseContainer(
            generation.getTestcontainer().getDatabase(),
            generation.getTestcontainer().getImageTag()
        );
        databaseContainer.start();
        try {

            DataSource dataSource = newDataSource(databaseContainer, generation.getTestcontainer().getJdbcQuery());

            int migrationsCounter = 0;
            migrationsCounter += executeFlywayMigrations(dataSource, generation.getFlyway());
            migrationsCounter += executeLiquibaseMigrations(dataSource, generation.getLiquibase());
            if (migrationsCounter == 0) {
                logger.warn("Nothing to generate - no migrations were executed");
                return;
            } else {
                logger.info(migrationsCounter + " migrations were executed");
            }

            jooqGenerateSources(dataSource, generation);

        } finally {
            databaseContainer.stop();
        }
    }

    private JdbcDatabaseContainer newDatabaseContainer(String database, @Nullable String imageTag) {
        JdbcDatabaseContainerProvider databaseContainerProvider = getDatabaseContainerProvider(database);
        if (imageTag == null || imageTag.isEmpty()) {
            return databaseContainerProvider.newInstance();
        } else {
            return databaseContainerProvider.newInstance(imageTag);
        }
    }

    private JdbcDatabaseContainerProvider getDatabaseContainerProvider(String database) {
        return StreamSupport.stream(ServiceLoader.load(JdbcDatabaseContainerProvider.class).spliterator(), false)
            .filter(it -> it.supports(database))
            .findAny()
            .orElseThrow(() -> new IllegalArgumentException(format(
                "%s can't be found for database %s",
                JdbcDatabaseContainerProvider.class.getSimpleName(),
                database
            )));
    }

    private DataSource newDataSource(JdbcDatabaseContainer databaseContainer, String jdbcQuery) {
        return new DataSource() {
            @Override
            public Connection getConnection() throws SQLException {
                return databaseContainer.createConnection(jdbcQuery);
            }

            @Override
            public Connection getConnection(String username, String password) throws SQLException {
                return getConnection();
            }

            @Override
            public java.util.logging.Logger getParentLogger() {
                return java.util.logging.Logger.getAnonymousLogger().getParent();
            }

            @Override
            public int getLoginTimeout() {
                return 10;
            }

            @Override
            @SuppressWarnings("unchecked")
            public <T> T unwrap(Class<T> iface) throws SQLException {
                if (isWrapperFor(iface)) {
                    return (T) this;
                } else {
                    throw new SQLException("DataSource does not implement " + iface);
                }
            }

            @Override
            public boolean isWrapperFor(Class<?> iface) {
                return iface.isInstance(this);
            }

            @Override
            public PrintWriter getLogWriter() throws SQLException {
                throw new SQLFeatureNotSupportedException("getLogWriter");
            }

            @Override
            public void setLogWriter(PrintWriter out) throws SQLException {
                throw new SQLFeatureNotSupportedException("setLogWriter");
            }

            @Override
            public void setLoginTimeout(int seconds) throws SQLException {
                throw new SQLFeatureNotSupportedException("setLoginTimeout");
            }
        };
    }

    private int executeFlywayMigrations(DataSource dataSource, JooqFlyway jooqFlyway) {
        if (jooqFlyway.getLocations().isEmpty()) {
            logger.debug("Skipping Flyway migrations, as no locations are set");
            return 0;
        }

        logger.info("Executing Flyway migrations with these locations: " + join(", ", jooqFlyway.getLocations()));
        FluentConfiguration configuration = Flyway.configure()
            .dataSource(dataSource)
            .locations(jooqFlyway.getLocations().toArray(new String[0]))
            .placeholders(jooqFlyway.getPlaceholders())
            .schemas(jooqFlyway.getSchemas().toArray(new String[0]))
            .table(jooqFlyway.getHistoryTableName())
            .encoding(migrationsEncoding);

        Flyway flyway = new Flyway(configuration);

        return flyway.migrate();
    }

    @SneakyThrows
    private int executeLiquibaseMigrations(DataSource dataSource, JooqLiquibase jooqLiquibase) {
        if (jooqLiquibase.getChangeLogFile() == null) {
            logger.debug("Skipping Liquibase migrations, as ChangeLog file isn't set");
            return 0;
        }

        logger.info("Executing Liquibase migrations with ChangeLog: " + jooqLiquibase.getChangeLogFile());
        ResourceAccessor resourceAccessor = new FileSystemResourceAccessor(project.getBasedir().getAbsolutePath());
        JdbcConnection connection = new JdbcConnection(dataSource.getConnection());
        try {
            liquibase.database.Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(connection);
            if (isNotEmpty(jooqLiquibase.getDefaultSchemaName())) {
                database.setDefaultSchemaName(jooqLiquibase.getDefaultSchemaName());
            }
            database.setDatabaseChangeLogTableName(jooqLiquibase.getChangeLogTableName());
            database.setDatabaseChangeLogLockTableName(jooqLiquibase.getChangeLogLockTableName());

            Liquibase liquibase = new Liquibase(jooqLiquibase.getChangeLogFile(), resourceAccessor, database);
            jooqLiquibase.getParameters().forEach(liquibase::setChangeLogParameter);

            AtomicInteger migrationsCounter = new AtomicInteger(0);
            liquibase.setChangeExecListener(new AbstractChangeExecListener() {
                @Override
                public void ran(Change change, ChangeSet changeSet, DatabaseChangeLog changeLog, liquibase.database.Database database) {
                    migrationsCounter.incrementAndGet();
                }
            });

            liquibase.update(join(", ", jooqLiquibase.getContexts()));

            return migrationsCounter.get();

        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    private void jooqGenerateSources(DataSource dataSource, JooqGeneration generation) {
        File targetDirectory = ofNullable(generation.getOutputDirectory()).orElse(this.outputDirectory).getAbsoluteFile();
        forceDeleteRecursively(targetDirectory);
        createDirectories(targetDirectory);
        synchronized (project) {
            project.addCompileSourceRoot(targetDirectory.getPath());
        }

        Set<String> schemas = new HashSet<>();
        schemas.addAll(generation.getFlyway().getSchemas());
        ofNullable(generation.getLiquibase().getDefaultSchemaName()).ifPresent(schemas::add);
        if (schemas.isEmpty()) {
            ofNullable(getDefaultSchemaName(dataSource)).ifPresent(schemas::add);
        }

        String packageName = ofNullable(generation.getPackageName()).filter(StringUtils::isNotEmpty)
            .orElseGet(() -> (project.getGroupId() + '.' + project.getArtifactId()).replaceAll("[^\\w.]", ""));

        Configuration configuration = new Configuration().withGenerator(
            new Generator()
                .withDatabase(new Database()
                    .withInputSchema(schemas.size() == 1 ? schemas.iterator().next() : "")
                    .withIncludes(
                        schemas.stream()
                            .map(Pattern::quote)
                            .map(pattern -> pattern + "\\..+")
                            .collect(joining(" | "))
                    )
                    .withExcludes(
                        Stream.of(
                            generation.getFlyway().getHistoryTableName(),
                            generation.getLiquibase().getChangeLogTableName(),
                            generation.getLiquibase().getChangeLogLockTableName()
                        ).map(Pattern::quote).collect(joining(" | "))
                    )
                )
                .withTarget(new Target()
                    .withDirectory(targetDirectory.getPath())
                    .withPackageName(packageName)
                    .withEncoding(generatedSourcesEncoding)
                )
                .withGenerate(generation.getJooq())
        );

        GenerationTool generator = new GenerationTool();
        generator.setDataSource(dataSource);

        generator.run(configuration);
    }

    @Nullable
    @SneakyThrows
    private static String getDefaultSchemaName(DataSource dataSource) {
        JdbcConnection connection = new JdbcConnection(dataSource.getConnection());
        try {
            liquibase.database.Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(connection);
            return database.getDefaultSchemaName();

        } finally {
            connection.close();
        }
    }

}
