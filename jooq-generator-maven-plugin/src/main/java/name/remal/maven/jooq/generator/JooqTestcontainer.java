package name.remal.maven.jooq.generator;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import name.remal.maven.annotations.Nullable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import static lombok.AccessLevel.PRIVATE;

@Data
@NoArgsConstructor
@FieldDefaults(level = PRIVATE)
public class JooqTestcontainer {

    @NotEmpty
    String database = "";

    @Nullable
    String imageTag;

    @NotNull
    String jdbcQuery = "";

}
