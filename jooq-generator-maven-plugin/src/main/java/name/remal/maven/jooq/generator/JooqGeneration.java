package name.remal.maven.jooq.generator;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import name.remal.maven.annotations.Nullable;
import org.jooq.meta.jaxb.Generate;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.File;

import static lombok.AccessLevel.PRIVATE;

@Data
@NoArgsConstructor
@FieldDefaults(level = PRIVATE)
public class JooqGeneration {

    @Valid
    @NotNull
    JooqTestcontainer testcontainer = new JooqTestcontainer();

    @Valid
    @NotNull
    JooqFlyway flyway = new JooqFlyway();

    @Valid
    @NotNull
    JooqLiquibase liquibase = new JooqLiquibase();

    @Nullable
    File outputDirectory;

    @Nullable
    String packageName;

    /**
     * See <a href="https://www.jooq.org/doc/current/manual/code-generation/codegen-advanced/codegen-config-generate/">https://www.jooq.org/doc/current/manual/code-generation/codegen-advanced/codegen-config-generate/</a>
     */
    @Valid
    @NotNull
    Generate jooq = new Generate();

}
