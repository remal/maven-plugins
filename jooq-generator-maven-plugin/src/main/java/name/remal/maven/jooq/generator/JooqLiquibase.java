package name.remal.maven.jooq.generator;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import name.remal.maven.annotations.Nullable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static lombok.AccessLevel.PRIVATE;

@Data
@NoArgsConstructor
@FieldDefaults(level = PRIVATE)
public class JooqLiquibase {

    @Nullable
    @Pattern(regexp = ".+", message = "{javax.validation.constraints.NotEmpty.message}")
    String changeLogFile;

    @NotNull
    List<String> contexts = new ArrayList<>();

    @NotNull
    Map<String, String> parameters = new HashMap<>();

    @Nullable
    String defaultSchemaName;

    @NotEmpty
    String changeLogTableName = "DATABASECHANGELOG";

    @NotEmpty
    String changeLogLockTableName = "DATABASECHANGELOGLOCK";

}
