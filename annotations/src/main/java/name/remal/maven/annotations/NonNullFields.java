package name.remal.maven.annotations;

import javax.annotation.Nonnull;
import javax.annotation.meta.TypeQualifierDefault;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PACKAGE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(PACKAGE)
@Retention(RUNTIME)
@Documented
@Nonnull
@TypeQualifierDefault(FIELD)
public @interface NonNullFields {
}
