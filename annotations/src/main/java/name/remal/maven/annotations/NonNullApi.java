package name.remal.maven.annotations;

import javax.annotation.Nonnull;
import javax.annotation.meta.TypeQualifierDefault;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(PACKAGE)
@Retention(RUNTIME)
@Documented
@Nonnull
@TypeQualifierDefault({METHOD, PARAMETER})
public @interface NonNullApi {
}
