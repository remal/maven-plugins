package name.remal.maven.annotations;

import javax.annotation.Nonnull;
import javax.annotation.meta.TypeQualifierNickname;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD, PARAMETER, FIELD})
@Retention(RUNTIME)
@Documented
@Nonnull
@TypeQualifierNickname
public @interface NonNull {
}
