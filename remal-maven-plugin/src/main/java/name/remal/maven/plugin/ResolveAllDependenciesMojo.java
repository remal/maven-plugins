package name.remal.maven.plugin;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.logging.Logger;

import static name.remal.maven.utils.MavenSessionUtils.forEachProject;
import static org.apache.maven.plugins.annotations.LifecyclePhase.VALIDATE;
import static org.apache.maven.plugins.annotations.ResolutionScope.TEST;

@Mojo(name = "resolve-all-dependencies", requiresDependencyResolution = TEST, defaultPhase = VALIDATE, threadSafe = true)
public class ResolveAllDependenciesMojo extends AbstractMojo {

    @Parameter(property = "resolve-all-dependencies.skip", defaultValue = "false")
    private boolean skip;

    @Parameter(required = true, readonly = true, defaultValue = "${project}")
    @SuppressWarnings("NullableProblems")
    private MavenProject project;

    @Parameter(required = true, readonly = true, defaultValue = "${session}")
    @SuppressWarnings("NullableProblems")
    private MavenSession session;

    @Component
    @SuppressWarnings("NullableProblems")
    private Logger logger;

    @Override
    public void execute() {
        if (skip) {
            logger.info("Skipping plugin execution");
            return;
        }

        processProject(project);
        forEachProject(session, ResolveAllDependenciesMojo::processProject);
    }

    private static void processProject(MavenProject project) {
        project.addLifecyclePhase("compile");
        project.addLifecyclePhase("test-compile");
    }

}
