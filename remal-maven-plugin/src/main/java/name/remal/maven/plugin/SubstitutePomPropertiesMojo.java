package name.remal.maven.plugin;

import lombok.SneakyThrows;
import lombok.val;
import org.apache.commons.text.StringSubstitutor;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.lifecycle.internal.MojoDescriptorCreator;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.plugin.PluginParameterExpressionEvaluator;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.logging.Logger;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.nio.file.Files.readAllBytes;
import static java.nio.file.Files.write;
import static name.remal.maven.utils.FileUtils.createParentDirectories;
import static org.apache.maven.plugins.annotations.LifecyclePhase.PACKAGE;

@Mojo(name = "substitute-pom-properties", defaultPhase = PACKAGE, threadSafe = true)
public class SubstitutePomPropertiesMojo extends AbstractMojo {

    @Parameter(required = true, defaultValue = "${project.build.directory}/pom-substituted.xml")
    @SuppressWarnings("NullableProblems")
    private File targetFile;

    @Parameter(required = true, defaultValue = "UTF-8")
    @SuppressWarnings("NullableProblems")
    private String pomEncoding;

    @Parameter
    @SuppressWarnings("NullableProblems")
    private List<String> properties;

    @Parameter(property = "substitute-pom-properties.skip", defaultValue = "false")
    private boolean skip;


    @Parameter(required = true, readonly = true, defaultValue = "${session}")
    @SuppressWarnings("NullableProblems")
    private MavenSession session;

    @Parameter(required = true, readonly = true, defaultValue = "${project}")
    @SuppressWarnings("NullableProblems")
    private MavenProject project;


    @Component
    @SuppressWarnings("NullableProblems")
    private Logger logger;

    @Component
    @SuppressWarnings("NullableProblems")
    private MojoDescriptorCreator mojoDescriptorCreator;


    @Override
    @SneakyThrows
    public void execute() {
        if (skip) {
            logger.info("Skipping plugin execution");
            return;
        }

        if (properties.isEmpty()) {
            logger.info("No properties set for substitution. Skipping execution.");
            return;
        }

        Map<String, String> props = new LinkedHashMap<>();
        synchronized (session) {
            val mojoDescriptor = mojoDescriptorCreator.getMojoDescriptor("remal:substitute-pom-properties", session, project);
            val mojoExecution = new MojoExecution(mojoDescriptor);

            val expressionEvaluator = new PluginParameterExpressionEvaluator(session, mojoExecution);
            for (String prop : properties) {
                Object value = expressionEvaluator.evaluate("${" + prop + "}");
                props.put(prop, value != null ? value.toString() : "");
            }
        }

        File originalPomFile = project.getFile().getAbsoluteFile();
        logger.debug("Substitute properties in " + originalPomFile + ": " + props);

        String content = new String(readAllBytes(originalPomFile.toPath()), pomEncoding);
        String substitutedContent = new StringSubstitutor(props).replace(content);
        if (substitutedContent.equals(content)) {
            return;
        }

        File outputFile = this.targetFile.getAbsoluteFile();
        createParentDirectories(outputFile);
        write(outputFile.toPath(), substitutedContent.getBytes(pomEncoding));

        project.setPomFile(outputFile);
    }

}
